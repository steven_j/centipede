#import "string-helpers/lib/string.mligo" "String_helper"

let head = "('><')"

let tail = "("

let leg = "_,"

let invoke (n : nat) : string = tail ^ String_helper.repeat(leg, n) ^ head
