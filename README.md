# centipede

This lib allows to invoke an ASCII centipede.

## Usage 

```mligo
#import "centipede/centipede.mligo" "Centipede"

Centipede.invoke 3n
```
